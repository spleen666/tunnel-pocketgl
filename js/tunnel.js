{
    //vertexShaderFile: "shaders/vert.glsl",
    fragmentShaderFile: "../shaders/frag.glsl",

    //fluidWidth: true,
    copyright:"i@spleen.noo.name",
    
    editorTheme: "dark", showTabs: true,tabTextColor:"#fff", tabColor:"#fdfdfd",

    uniforms: [
      { 
        type: "float", 
        value: 3.0, 
        min: 0.0, 
        max: 8.0, 
        name: "bump_h", 
        GUIName: "bump"
      },
      { 
        type: "float", 
        value: 0.005, 
        min: 0.0, 
        max: 0.010, 
        name: "rel",
        GUIName: "relief"
      },
      { 
        type: "float", 
        value: 0.1, 
        min: 0.0, 
        max: .20, 
        name: "speed",
        GUIName: "speed"
      }
    ],

    textures: [
      {
          url: "../textures/tx1.jpg",
          wrap: "repeat", // repeat (default), clamp
          filter: "nearest",
          name: "texture0"
      },
       {
          url: "../textures/tx1_height.png",
          wrap: "repeat", // repeat (default), clamp
          filter: "nearest",
          name: "texture1"
      }
      ],

      animated: true,
      playButtons: true

}
